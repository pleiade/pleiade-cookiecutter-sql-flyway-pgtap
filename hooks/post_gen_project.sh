#!/bin/bash

case "{{ cookiecutter.input_format }}" in
    PostgreSQL)
	cp "{{ cookiecutter.input_sql }}" migrations/V1__schema.sql ;;
    MySQL)
	cp "{{ cookiecutter.input_sql }}" convert/input_data.sql ;;
    MySQL-convert)
	cp "{{ cookiecutter.input_sql }}" convert/input_data.sql
	(cd convert && docker-compose run dump) ;;
esac
