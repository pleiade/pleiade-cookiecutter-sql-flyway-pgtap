# {{cookiecutter.project_name}}

{{cookiecutter.short_description}}

# Database reformating

The source database may require reformating, either to improve the data model or to remove confusion in its interpretation. In order to guarantee the correctness and the tracability of these modifications, we use:

1. **Migration** to reformat the database schema and data
2. **Unit Testing** of the resulting database

This organization makes in easy to create individual GitLab issues for problems in the database and to resolve them using merge requests, with continuous validation guaranteed by the GitLab CI service.

*This project was generated with [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) using [Pleiade](https://team.inria.fr/pleiade/)'s [cookiecutter-sql-flyway-pgtap](https://gitlab.inria.fr/pleiade/pleiade-cookiecutter-sql-flyway-pgtap/) template.*

### Migrations with Flyway

[Flyway](https://flywaydb.org) is a lifecycle management tool for database schemas. Every modification of the schema is specified in an SQL file. File names follow a semantic versioning convention and are applied in a strict order.

The migrations in the [migrations/](migrations/) directory are applied and validated by the step `tests:migrations` in [.gitlab.yml](.gitlab.yml). We use database server instances in ephemeral Docker containers to ensure test independence.

### Unit Tests with pgTAP

[pgTAP](https://pgtap.org) specifies unit tests in SQL, optionally in xUnit style, and presents the results in TAP format. Each set of unit tests is specified in a **plan**. Optionally, tests that concern work in progress can be declared “todo” so that their individual failure does not cause the ensuite suite to fail.

The tests in the [tests/pgtap/](tests/pgtap/) directory are executed by the step `tests:unit` in [.gitlab.yml](.gitlab.yml).

In order to use pgTAP, the database must be in PostgreSQL format. The script [orig/conversion-mysql-postgresql.sh](orig/conversion-mysql-postgresql.sh) shows an example of database conversion from MySQL:

```
(cd orig && bash conversion-mysql-postgresql.sh) > ../migrations/V1__data.sql
```
