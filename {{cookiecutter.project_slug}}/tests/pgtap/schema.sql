BEGIN;

-- Declare plan containing 1 test
SELECT plan(2);

-- does tablespace postgres exist?
SELECT has_tablespace('postgres', 'Default tablespace exists');

-- does schema {{cookiecutter.database_name}} exist?
SELECT has_schema('{{ cookiecutter.database_name }}', 'Default schema exists');

-- Run test plan
SELECT * FROM finish();
ROLLBACK;
