gitlab-runner exec docker "$@" \
  --docker-volumes $PWD/output:/builds/output \
  --docker-pull-policy never \
  --env CI_REGISTRY_IMAGE=registry.gitlab.inria.fr/{{ cookiecutter.project_slug }}/{{ cookiecutter.project_slug }} \
  --env CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)
